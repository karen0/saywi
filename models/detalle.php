<?php

class detas
{

    private $id_detalle;
    private $cantidad;
    private $id_es_fk;
    private $id_bor_fk;
    private $id_coti_fk;
    private $pdo;

    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo constructor


    public function getAll()
    {
        try {
            $strSql = 'SELECT * FROM detalle';
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo getAlls

    public function newdetalle($data)
    {
        try {
            $this->pdo->insert('detalle', $data);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    } // fin metodo newUser

    public function getById($id_detalle)
    {
        try {
            $strSql = 'SELECT * FROM detalle WHERE id_detalle = :id_detalle';
            $array = ['id_detalle' => $id_detalle];
            $query = $this->pdo->select($strSql, $array);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
   
    public function editdetalle($data)
    {
        try {
            $srtWhere = 'id_detalle = ' . $data['id_detalle'];
            $this->pdo->update('detalle', $data, $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function deletedetalle($data)
    {
        try {
            $srtWhere = 'id_detalle = ' . $data['id_detalle'];
            $this->pdo->delete('detalle', $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
} # fin clase Role
