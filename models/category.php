<?php

class category
{

    private $id_categoria;
    private $nombre;
    private $imagen;
    private $id_es_fk;
    private $pdo;

    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo constructor


    public function getAll()
    {
        try {
            $strSql = 'SELECT c.*,  s.nombre as status FROM categoria c INNER JOIN  status s ON s.id_estado= c.id_es_fk';
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo getAlls
    public function getAllb()
    {
        try {
            $strSql = 'SELECT b.id_bordado,b.nombre,b.imagen,b.medida, b.puntadas,b.precio FROM bordado b INNER JOIN categoria c on c.id_categoria = b.id_cat_fk ';
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo getAlls
    public function newcategory($data)
    { 
        try {
            $this->pdo->insert('categoria', $data);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
        
        } // fin metodo newUser

    public function getById($id_categoria)
    {
        try {
            $strSql = 'SELECT * FROM categoria WHERE id_categoria = :id_categoria';
            $array = ['id_categoria' => $id_categoria];
            $query = $this->pdo->select($strSql, $array);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getByIdm($id_categoria)
    {
        try {
            $strSql = "SELECT * FROM categoria WHERE id_categoria = :id_categoria";
            $array = ['id_categoria' => $id_categoria];
            $query = $this->pdo->select($strSql, $array);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
   
    public function nuevaCotizacion($idPersona, $fecha_entrega){
         try {
            $conexion = new  mysqli("localhost","root","","saywi_20");
            $query = "INSERT INTO `cotizacion`( `fecha_pedi`, `fecha_entrega`, `observacion`,`id_es_fk`,`id_per_fk`) VALUES (NOW(),'$fecha_entrega','default',1,'$idPersona')";
            $resultado=$conexion->query($query);
            $id_cotizacion =  "SELECT MAX(id_cotizacion) from cotizacion";
            $cotizacion =$conexion->query($id_cotizacion);
            $id_cotizacion = $cotizacion->fetch_row();
            return $id_cotizacion[0];
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    
    public function getidCotizacion($id){
        try {
            $_fecha= date_default_timezone_get  ();
           $conexion = new  mysqli("localhost","root","","saywi_20");
           $query = "SELECT id_cotizacion FROM cotizacion WHERE fecha_pedi ='$_fecha' and id_per_fk='$id'";
           $resultado=$conexion->query($query);
            return $resultado;
       } catch (PDOException $e) {
           die($e->getMessage());
       }
   }

   public function insertar($id_cotizacion, $bordado)
   {
       try {
            $conexion = new  mysqli("localhost","root","","saywi_20");
            $query = 'INSERT INTO detalle (cantidad,valor,id_es_fk,id_bor_fk,id_coti_fk) VALUES ('.$bordado["cantidad"].','.$bordado["precio"].',1,'.$bordado["id_bordado"].','.$id_cotizacion.')';
            $resultado = $conexion->query($query);
            var_dump($query,$resultado);
          }catch (PDOException $e) {
            die($e->getMessage());
          }
      
   }
    public function editcategory($data)
    {
        try {
            $srtWhere = 'id_categoria = ' . $data['id_categoria'];
            $this->pdo->update('categoria', $data, $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
	
    }

    public function deletecategory($data)
    {
        try {
            $srtWhere = 'id_categoria = ' . $data['id_categoria'];
            $this->pdo->delete('categoria', $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    
     # fin metodo getAlls





} # fin clase Role
