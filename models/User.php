<?php

class User
{

  private $id_usuario;
  private $usuario;
  private $contrasena;
  private $tipo_usuario;
  private $id_es_kf;
  private $id_per_fk;
  private $pdo;

  public function __construct()
  {
    try {
      $this->pdo = new Database;
    } catch (PDOException $e) {
      die($e->getMessage());
    }
  }

  public function getAll()
  {
    try {
      $strSql = "SELECT u.*, p.nombre as personas, s.nombre as status, p.nombre as personas FROM usuarios u INNER JOIN personas p on p.id_personas = u.id_per_fk INNER JOIN status s ON s.id_estado= u.id_es_fk";
      $query = $this->pdo->select($strSql);
      return $query;
    } catch (PDOException $e) {
      die($e->getMessage());
    }
  }

  public function newUser($data)
  {
    try {
      $this->pdo->insert('usuarios', $data);
    } catch (PDOException $e) {
      die($e->getMessage());
    }
  } // fin metodo newMovie

  public function getById($id_usuario)
  {
    try {
      $strSql = 'SELECT * FROM usuarios WHERE id_usuario = :id_usuario';
      $array = ['id_usuario' => $id_usuario];
      $query = $this->pdo->select($strSql, $array);
      return $query;
    } catch (PDOException $e) {
      die($e->getMessage());
    }
  }

  public function editUser($data)
  {
    try {
      $srtWhere = 'id_usuario = ' . $data['id_usuario'];
      $this->pdo->update('usuarios', $data, $srtWhere);
    } catch (PDOException $e) {
      die($e->getMessage());
    }
  }

  public function deleteUser($data)
  {
    try {
      $srtWhere = 'id_usuario = ' . $data['id_usuario'];
      $this->pdo->delete('usuarios', $srtWhere);
    } catch (PDOException $e) {
      die($e->getMessage());
    }
  }

  public function getLastId()
  {
    try {
      $lastId = $this->pdo->lastId('usuarios');
      return $lastId;
    } catch (PDOException $e) {
      die($e->getMessage());
    }
  }

} # fin clase
