<?php

class Quote
{

    private $id_solicitud;
    private $fecha_pedi;
    private $fecha_entrega;
    private $observacion;
    private $id_es_fk;
    private $id_per_fk;
    private $pdo;

    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo constructor


    public function getAll()
    {
        try {
            $strSql = 'SELECT * FROM cotizacion';
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo getAlls
    public function newQuote($data)
    {
        try {
            $this->pdo->insert('cotizacion', $data);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    } // fin metodo newUser
    
} # fin clase Role
