<?php


class embroidery
{

    private $id_bordado;
    private $nombre;
    private $imagen;
    private $medida;
    private $puntadas;
    private $precio;
    private $id_es_fk;
    private $id_cat_fk;
    private $pdo;

    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo constructor


    public function getAll()
    {
        try {
            $strSql = "SELECT b.*, c.nombre as categoria, s.nombre as status, c.nombre as categoria FROM bordado b INNER JOIN categoria c on c.id_categoria = b.id_cat_fk INNER JOIN status s ON s.id_estado= b.id_es_fk";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo getAlls
    public function getAllm($id_categoria)
    {
        try {
            $array = ['id_categoria' => $id_categoria];
            $strSql = "SELECT b.id_bordado,b.nombre,b.imagen,b.medida, b.puntadas,b.precio FROM bordado b INNER JOIN categoria c on c.id_categoria = b.id_cat_fk where c.id_categoria='$id_categoria'";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } 
    public function getAllp($id_bordado)
    {
        try {
            $strSql = "SELECT d.cantidad b.id_bordado FROM detalle d INNER JOIN bordado b on b.id_bordado = d.id_bor_fk where b.id_bordado='$id_bordado'";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } 
    public function nuevaCoti($id){
        try {
           $conexion = new  mysqli("localhost","root","","saywi_20");
           $query = "INSERT INTO `detalle`( `cantidad`, `id_es_fk`, `id_bor_fk`, `id_coti_fk`) VALUES (1,1,'$id',4)";
           $resultado=$conexion->query($query);

       } catch (PDOException $e) {
           die($e->getMessage());
       }
   }
   public function getByIdp($id)
   {
       try {
           $strSql = "SELECT d.cantidad,d.id_bor_fk FROM detalle d INNER JOIN bordado b on b.id_bordado = d.id_bor_fk where d.id_bor_fk='$id'";
           $array = ['id' => $id];
           $query = $this->pdo->select($strSql, $array);
           return $query;
       } catch (PDOException $e) {
           die($e->getMessage());
       }
   }
    public function newbordado($data)
    {
        try {
            $this->pdo->insert('bordado', $data);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    } // fin metodo newUser

    public function getById($id_bordado)
    {
        try {
            $strSql = 'SELECT * FROM bordado WHERE id_bordado = :id_bordado';
            $array = ['id_bordado' => $id_bordado];
            $query = $this->pdo->select($strSql, $array);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
   
    public function editbordado($data)
    {
        try {
            $srtWhere = 'id_bordado = ' . $data['id_bordado'];
            $this->pdo->update('bordado', $data, $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
	
    }

    public function deletebordado($data)
    {
        try {
            $srtWhere = 'id_bordado = ' . $data['id_bordado'];
            $this->pdo->delete('bordado', $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    

    
     # fin metodo getAlls





} # fin clase Role
