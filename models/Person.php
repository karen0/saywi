<?php

class Person
{

    private $id_personas;
    private $documento;
    private $nombre;
    private $telefono;
    private $correo;
    private $id_es_fk;   
    private $pdo;

    public function __construct()
    {
        try {
            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    } # fin metodo constructor


    public function getAll()
    {
        try {
            $strSql = "SELECT p.*, s.nombre as status FROM personas p INNER JOIN status s ON s.id_estado= p.id_es_fk";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    } # fin metodo getAll

    public function newPerson($data)
    {
        try {
            $this->pdo->insert('personas', $data);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    } // fin metodo newUser

    public function getById($id_personas)
    {
        try {
            $strSql = 'SELECT * FROM personas WHERE id_personas = :id_personas';
            $array = ['id_personas' => $id_personas];
            $query = $this->pdo->select($strSql, $array);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function editPerson($data)
    {
        try {
            $srtWhere = 'id_personas = ' . $data['id_personas'];
            $this->pdo->update('personas', $data, $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function deletePerson($data)
    {
        try {
            $srtWhere = 'id_personas = ' . $data['id_personas'];
            $this->pdo->delete('personas', $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
} # fin clase Personas
