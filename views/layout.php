<?php
session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

} else {
//    echo "Inicia Sesion para acceder a este contenido.<br>";
//    echo "<br><a href='index.html'>Login</a>";
//    echo "<br><br><a href='index.html'>Registrarme</a>";
   header('Location: ?controller=user&method=hola');//redirige a la página de login si el usuario quiere ingresar sin iniciar sesion


exit;
}

$now = time();

if($now > $_SESSION['expire']) {
session_destroy();
header('Location: ?controller=user&method=hola');//redirige a la página de login, modifica la url a tu conveniencia

exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="assets/img/icono_saywi.ico">
  <title>SAYWI</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="assets/vendor/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="assets/vendor/base/vendor.bundle.base.css">
 
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="assets/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="assets/css/stylePlantilla.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="assets/img/favicon.png" />
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="navbar-brand-wrapper d-flex justify-content-center">
        <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">  
          <a class="navbar-brand brand-logo" href="index.html"><img src="assets/img/logo-small.png" alt="logo"/></a>
          <a class="navbar-brand brand-logo-mini" href="index.html"><img src="assets/img/logo-small.png" alt="logo"/></a>
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-sort-variant"></span>
          </button>
        </div>  
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <ul class="navbar-nav mr-lg-4 w-100">
          <li class="nav-item nav-search d-none d-lg-block w-100">
            
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
   
          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
              <img src="assets/img/gerente-sa.jpg" alt="profile"/>
              <span class="nav-profile-name">Gerente</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
              <a class="dropdown-item" href="?controller=user&method=logout">
                <i class="mdi mdi-logout text-primary"></i>
                Cerrar Sesion
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          
          <li class="nav-item">
            <a class="nav-link" href="?controller=user&method=index">
            <i class="mdi mdi-account-circle"></i>
              <span class="mdi "> Usuarios </span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="?controller=person&method=index">
            <i class="mdi mdi-account-multiple"></i>
              <span class="mdi "> Personas</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="?controller=status&method=index">
            <i class="mdi mdi-clipboard-text"></i>
              <span class="mdi "> Estados</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="?controller=category&method=index">
            <i class="mdi mdi-grid"></i>
              <span class="mdi "> Categorias</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="?controller=embroidery&method=index">
            <i class="mdi mdi-postage-stamp"></i>
              <span class="mdi "> Bordados</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="?controller=quote&method=index">
            <i class="mdi mdi-cart"></i>
              <span class="mdi "> Cotizaciones</span>
            </a>
          </li>

          <!-- <li class="nav-item">
            <a class="nav-link" href="pages/tables/basic-table.html">
              <i class="mdi mdi-grid-large menu-icon"></i>
              <span class="menu-title">TABLAS</span>
            </a>
          </li>
        </ul> -->
      </nav>
    
      <!-- partial -->
           </div>
           
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020 <a href="https://www.urbanui.com/" target="_blank">SAYWI</a>.</span>
            
          </div>
        </footer>

  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="assets/vendor/base/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="assets/vendor/chart.js/Chart.min.js"></script>
  <script src="assets/vendor/datatables.net/jquery.dataTables.js"></script>
  <script src="assets/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="assets/js/off-canvas.js"></script>
  <script src="assets/js/hoverable-collapse.js"></script>
  <script src="assets/js/template.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="assets/js/dashboard.js"></script>
  <script src="assets/js/data-table.js"></script>
  <script src="assets/js/jquery.dataTables.js"></script>
  <script src="assets/js/dataTables.bootstrap4.js"></script>
  <!-- End custom js for this page-->
</body>

</html>


