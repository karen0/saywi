<!--
Author: W3layouts
Author URL: http://w3layouts.com
-->
<?php
session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

} else {
//    echo "Inicia Sesion para acceder a este contenido.<br>";
//    echo "<br><a href='index.html'>Login</a>";
//    echo "<br><br><a href='index.html'>Registrarme</a>";
   header('Location: ?controller=user&method=hola');//redirige a la página de login si el usuario quiere ingresar sin iniciar sesion


exit;
}

$now = time();

if($now > $_SESSION['expire']) {
session_destroy();
header('Location: ?controller=user&method=hola');//redirige a la página de login, modifica la url a tu conveniencia

exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SAYWI</title>
    <link rel="stylesheet" href="assets/css/styleTotal.css">
    <link href="//fonts.googleapis.com/css?family=Roboto:400,700,900&display=swap" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Nunito:400,600,700,800,900&display=swap" rel="stylesheet">
    <link rel="icon" href="assets/img/icono_saywi.ico">

</head>

<body>
    
    <section class="w3l-bootstrap-header">
        <nav class="navbar navbar-expand-lg navbar-light bg-primary py-lg-2 py-2 px-sm-0 px-2">
            <div class="container">
                <a class="navbar-brand" href="index.html"><img class="navbar-brand" src="assets/img/logo-small.png"></a>
                <!-- if logo is image enable this   
    <a class="navbar-brand" href="#index.html">
        <img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
    </a> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto">
                       <h4>Bienvenida</h4>
                         <li class="nav-item">
                            <h5 class="nav-link" href="index.html#contact"><?php echo ($_SESSION['usuario']); ?></h5>
                        </li>
                        <li class="nav-item">
                        <a href="?controller=user&method=logout" class="btn btn-outline-light btn-outline-action mt-5">Cerrar Sesión</a>
                        </li>
                    </ul>
                  
                  

                </div>
            </div>
        </nav>
    </section>

    <section class="w3l-carousel">
        <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active" style="background-image: url('assets/img/saco.jpg');">
                    <!-- <img src="assets/images/banner.jpg" class="d-block w-100" alt="..."> -->
                    <div class="carousel-caption container">
                        <h3 class="title-cover-9">Bienvenido</h3>
                        <p class="para-cover-9">Gracias por registrarte y usar nuestros servicios, aqui te mostraremos todos los diseños de bordados que tenemos a tu disposición 
                        Solo tienes que dar click en ver portafolio y empezar tu cotización</p>
                        <a href="?controller=category&method=catalogo" class="btn btn-primary btn-action mt-5">Ver Portfolio</a>
                    </div>
                </div>
                <div class="carousel-item" style="background-image: url('assets/img/Equipos.jpg')">
                    <!-- <img src="assets/images/banner-3.jpg" class="d-block w-100" alt="..."> -->
                    <div class="carousel-caption container">
                        <h3 class="title-cover-9">Equipo de Trabajo</h3>
                        <p class="para-cover-9">Nuestro equipo de trabajo estara siempre a tu disposicion asesorandote a la hora de escoger tu bordado y a resolver cualquier inquietud respecto a nuestros servicios.</p>
                        <a href="?controller=quote&method=quote" class="btn btn-primary btn-action mt-5">Ver Portfolio</a>
                    </div>
                </div>
                <div class="carousel-item" style="background-image: url('assets/img/maquina.jpg')">
                    <!-- <img src="assets/images/banner-2.jpg" class="d-block w-100" alt="..."> -->
                    <div class="carousel-caption container">
                        <h3 class="title-cover-9">Maquinas Y Materia Prima</h3>
                        <p class="para-cover-9">Contamos con maquinas de ultima generación y materiales elaborados con los mas altos niveles de calidad garantizando la durabilidad de nuestro producto.</p>
                        <a href="?controller=quote&method=quote" class="btn btn-primary btn-action mt-5">Ver Portfolio</a>
                    </div>
                </div>
                <div class="carousel-item" style="background-image: url('assets/img/nnn.jpg')">
                    <!-- <img src="assets/images/banner-1.jpg" class="d-block w-100" alt="..."> -->
                    <div class="carousel-caption container">
                        <h3 class="title-cover-9">Diseños</h3>
                        <p class="para-cover-9">Tenemos para ti diferentes tipos de diseño diseñados a tu gusto y preferencia, ademas de ello contamos con una gran diseñadora que estara dispuesta a diseñar un unico y original boradado para ti.</p>
                        <a href="?controller=quote&method=quote" class="btn btn-primary btn-action mt-5">Ver Portfolio</a>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
       </div>
    </section>

    
    

    
   <!-- //form-16-section -->
   <div class="w3l-grids-block-5" id="blog">
        <!-- grids block 5 -->
        <section id="grids5-block" class="py-5">
            <div class="container py-lg-3">
                <div class="section-title align-center text-center">
                    <h3 class="global-title text-secondary">Tendencia En Bordados</h3>
                </div>
                <div class="row">
                    <div class="grids5-info col-lg-4 col-md-6 mt-4">
                        <a href="blog-single.html"><img src="assets/img/bonita.jpg" alt=/></a>
                        <div class="blog-info">
                            <h4><a href="blog-single.html">Bordados Empresariales</a></h4>
                            <p>Te ofrecemos bordar infinidad de prendas y logotipos, nombres y dibujos..</p>
                        </div>
                    </div>
                    <div class="grids5-info col-lg-4 col-md-6 mt-4">
                        <a href="blog-single.html"><img src="assets/img/colegios.jpg" alt="" /></a>
                        <div class="blog-info">
                            <h4><a href="blog-single.html">Bordados Colegiales</a></h4>
                            <p>Elaboramos los escudos colegiales incluyendo las chaquetas de once.</p>
                        </div>
                    </div>
                    <div class="grids5-info col-lg-4 offset-md-3 offset-lg-0 col-md-6 mt-4">
                        <a href="blog-single.html"><img src="assets/img/team/6.jpg" alt="" /></a>
                        <div class="blog-info">
                            <h4><a href="blog-single.html">Bordados en General</a></h4>
                            <p>Bordamos y personalizamos tus prendas más rápido que nuestra competencia. .</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- // grids block 5 -->
    <!-- footer -->
    <section class="w3l-footer-29-main" id="footer">
        <div class="footer-29 py-5">
            <div class="container pb-lg-3">
                <div class="row footer-top-29">
                    <div class="col-lg-4 col-md-6 footer-list-29 footer-1 mt-md-4">
                        <h6 class="footer-title-29">Contáctanos</h6>
                        <ul>
                            <li>
                                <p><span class="fa fa-map-marker"></span> Barrio: Candelaria la Nueva</p>
                            </li>
                            <li><a href="tel:+57 321 355 9727"><span class="fa fa-phone"></span> +57 321 355 9727</a></li>
                            <li><a href="saywi20@gmail.com" class="mail"><span class="fa fa-envelope-open-o"></span>
                                saywi20@gmail.com</a></li>
                        </ul>

                    </div>


                    <div class="col-lg-2 col-md-6 footer-list-29 footer-4 mt-4">
                        <ul>
                            <h6 class="footer-title-29">Enlaces Rápidos</h6>
                            <li><a href="index.html">Inicio</a></li>
                            <li><a href="#about">Sobre Nosotros</a></li>
                            <li><a href="#portfolio">Catalogo</a>
                                <li><a href="#contact">Contáctanos</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row bottom-copies">
                    <p class="copy-footer-29 col-lg-8">© 2020 SAYWI. Todos los derechos reservados
                        <ul class="list-btm-29 col-lg-4">
                            <li><a href="#link">Politica de Privacidad</a></li>
                            <li><a href="#link">Terminos de servicio</a></li>

                        </ul>
                </div>
            </div>
        </div>
        <!-- move top -->
        <button onclick="topFunction()" id="movetop" class="bg-primary" title="Go to top">
        <span class="fa fa-angle-up"></span>
    </button>
        <script>
            // When the user scrolls down 20px from the top of the document, show the button
            window.onscroll = function() {
                scrollFunction()
            };

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("movetop").style.display = "block";
                } else {
                    document.getElementById("movetop").style.display = "none";
                }
            }

            // When the user clicks on the button, scroll to the top of the document
            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }
        </script>
        <!-- /move top -->
    </section>
    <!-- // footer -->

    <!-- jQuery -->
    <script src="assets/js/jquery-3.4.1.slim.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- disable body scroll which navbar is in active -->
    <script>
        $(function() {
            $('.navbar-toggler').click(function() {
                $('body').toggleClass('noscroll');
            })
        });
    </script>
    <!-- disable body scroll which navbar is in active -->

    <!-- jQuery-Photo-filter-lightbox-portfolio-plugin -->
    <script src="assets/js/jquery-1.7.2.js"></script>
    <script src="assets/js/jquery.quicksand.js"></script>
    <script src="assets/js/script.js"></script>
    <script src="assets/js/jquery.prettyPhoto.js"></script>
    <!-- jQuery-Photo-filter-lightbox-portfolio-plugin -->
</body>

</html>