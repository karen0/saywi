<?php
require('fpdf/fpdf.php');


class PDF extends FPDF
{
// Cabecera de página
function Header()
{

    
    // Arial bold 15
    $this->SetFont('Arial','B',18);
    // Movernos a la derecha
    $this->Cell(80);
    // Título
    $this->Cell(30,10,' Bordados Saywi ,Cotizacion',0,0,'C');
    // Salto de línea
    $this->Ln(20);

    $this->Cell(30,10,'Id',1,0,'C',0);
$this->Cell(50,10,'Nombre',1,0,'C',0);
$this->Cell(30,10,'Medida',1,0,'C',0);
$this->Cell(30,10,'Puntadas',1,0,'C',0);
$this->Cell(50,10,'Precio',1,1,'C',0);

}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,utf8_decode('Pagina ').$this->PageNo().'/{nb}',0,0,'C');
}
}

 require 'cn.php';
 $consulta= "SELECT id_detalle,b.nombre,b.medida,b.puntadas,b.precio,d.id_coti_fk FROM bordado b INNER JOIN detalle d on b.id_bordado=d.id_bor_fk ORDER BY d.id_detalle DESC
 LIMIT 1";
 $resultado=$mysqli->query($consulta);


   
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',16);



 while ($row=$resultado->fetch_assoc()){
$pdf->Cell(30,10,$row['id_detalle'],1,0,'C',0);
$pdf->Cell(50,10,$row['nombre'],1,0,'C',0);
$pdf->Cell(30,10,$row['medida'],1,0,'C',0);
$pdf->Cell(30,10,$row['puntadas'],1,0,'C',0);
$pdf->Cell(50,10,$row['precio'],1,1,'C',0);

 }
$pdf->Output();
?>