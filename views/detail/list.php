

<main class="container">

    <section class="col-md-12 text-center">
        <h1>LISTADO DE DETALLES</h1>

        <div class="col-md-12 m-2 d-flex justify-content-between">
            <h2>CATEGORÍAS</h2>
            <a class="btn btn-success" href="?controller=detalle&method=add"> Agregar<i class="fas fa-user-plus fa-lg"></i></a>
        </div>

        <section class="col-md-12 flex-nowrap">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Cantidad</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>

                <tbody>

                    <?php foreach ($categories as $detalle) : ?>
                    <tr>
                        <td> <?php echo $detalle->id_detalle ?> </td>
                        <td> <?php echo $detalle->cantidad ?></td>
                        <td> <?php echo $detalle->id_es_fk ?></td>
                        <td> <?php echo $detalle->id_bor_fk ?></td>
                        <td> <?php echo $detalle->id_soli_fk ?></td>
                        <td>
                            <a class="btn btn-primary mr-1" href="?controller=detalle&method=edit&id=<?php echo $detalle->id_detalle ?>">  <i class="mdi mdi-pencil-circle"></i></a>

                            <a class="btn btn-danger mr-2" href="?controller=detalle&method=delete&id=<?php echo $detalle->id_detalle ?>"> <i class="mdi mdi-close-circle"></i></a>
                        </td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </section>
    </section>
</main>