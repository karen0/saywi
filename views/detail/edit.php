

<main class="container">

    <div class="row">
        <h1 class="col-12 d-flex justify-content-center">DETALLE</h1>
    </div>

    <section class="row mt-5">

        <div class="card w-50 m-auto">

            <div class="card-header container">
                <h2 class="m-auto">EDITAR DETALLE</h2>
            </div>

            <div class="card-body">
                <form method="POST" action="?controller=detalle&method=update">

                    <input type="hidden" name="id_detalle" class="form-control" value="<?php echo $data[0]->id_detalle; ?>">

                    <div class="input-field col s12">
                        <label for="name">Cantidad</label>
                        <input type="number" name="cantidad" class="validate" value="<?php echo $data[0]->cantidad; ?>" pattern="[0-9]" required>
                    </div>
                <br>
                    <div class="input-field col s12">
                        <select class="form-control" name="id_es_fk" required>
                            <option value="">Seleccione el estado en el que se encuentra...</option>
                            <?php foreach ($status as $s) : ?>
                                <option value="<?php echo $s->id_estado ?>"></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                <br>
                    <div class="input-field col s12">
                        <select class="form-control" name="id_bor_fk" required>
                            <option value="">Seleccione la categoría del bordado...</option>
                            <?php foreach ($detalle as $s) : ?>
                                <option value="<?php echo $s->id_bor_fk?>"></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                <br>
                    <div class="input-field col s12">
                        <select class="form-control" name="id_soli_fk" required>
                            <option value="">Seleccione la solicitud en la que se encuentra...</option>
                            <?php foreach ($solicitud as $s) : ?>
                                <option value="<?php echo $s->id_soli_fk ?>"></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                <br>
                    <div class="form-group">
                        <button class="btn btn-primary">Guardar</button>
                    </div>
                </form>

            </div>

        </div>
    </section>
</main>