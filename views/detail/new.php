
<main class="container">

    <div class="row">
        <h1 class="col-12 d-flex justify-content-center">DETALLE</h1>
    </div>

    <section class="row mt-5">

        <div class="card w-50 m-auto">

            <div class="card-header container">
                <h5 class="m-auto">AGREGAR DETALLE</h5>
            </div>

            <div class="card-body">
                <form method="POST" action="?controller=detalle&method=save">

                    <div class="input-field col s12">
                        <input id="cantidad" class="validate" type="number" placeholder="Cantidad" pattern="[0-9]" required>
                        <label for="name">Cantidad</label>  
                            <div class="invalid-feedback">
                                Por favor introduzca la cantidad.
                            </div>
                            <div class="valid-feedback">
                                Correcto!
                            </div>                                      
                    </div>
                <br>   
                    <div class="input-field col s12">
                        <input  class="validate" type="text" name="id_bor_fk" placeholder="Categoria" pattern="[A-zA-Z ]*" required>
                        <label for="name">Bordado</label>
                    </div>
                    <div class="input-field col s12">
                        <input class="validate" type="text" name="id_soli_fk" placeholder="Categoria" pattern="[A-zA-Z ]*" required>
                         <label for="name">Solicitud</label>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">Guardar</button>
                    </div>
                </form>

            </div>

        </div>
    </section>
</main>