
<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
  <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
</head>
<main class="container">
<div class="formularios">
<div class="row">

    </div>

    <section class="row mt-5">

        <div class="card w-50 m-auto">

            <div class="card-header container">
                <h6 class="m-auto">AGREGAR ESTADO</h6>
            </div>

            <div class="card-body">
                <form method="POST" action="?controller=status&method=save">
                    <div class="input-field col s12">
                        <input  class="validate" type="text" name="nombre" pattern="[A-zA-Z ]*"  minlength="3"  tabindex="1" required placeholder="Estado">
                    </div>
                <br>

                    <div class="form-group">
                        <button class="btn btn-primary">Guardar</button>
                    </div>
                </form>

            </div>

        </div>
    </section>
</div>
</main>