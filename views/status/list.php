
<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
</head>
<main class="container">
<div class="content-wrapper">
    <section class="col-md-12 text-center">

        <div class="col-md-12 m-2 d-flex justify-content-between">
            <h4>ESTADOS</h4>
            <a class="btn btn-success" href="?controller=status&method=add"> Agregar <i class="mdi mdi-library-plus"></i></a>
        </div>

        <section class="col-md-12 flex-nowrap">
        <table  bgcolor="#d1d1d1"  class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th class="tablita">#</th>
                        <th class="tablita">Estado</th>
                        <th class="tablita">Acciones</th>
                    </tr>
                </thead>

                <tbody>

                    <?php foreach ($status as $state) : ?>
                    <tr>
                        <td> <?php echo $state->id_estado ?> </td>
                        <td> <?php echo $state->nombre ?></td>
                        <td>
                            <a href="?controller=status&method=edit&id_estado=<?php echo $state->id_estado ?>">   <i class="mdi mdi-pencil-circle"></i></a>
                            <a href="?controller=status&method=delete&id_estado=<?php echo $state->id_estado ?>"> <i class="mdi mdi-close-circle"></i></a>
  
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </section>
    </section>
</div>
</main>
