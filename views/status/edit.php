
<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
  <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
</head>
<main class="container">
<div class="formularios">



    <section class="row mt-5">

        <div class="card w-50 m-auto">

            <div class="card-header container">
                <h6  class="m-auto">EDITAR ESTADOS</h6>
            </div>

            <div class="card-body">
                <form   method="POST" action="?controller=status&method=update">

                    <input type="hidden" name="id_estado" class="form-control" value="<?php echo $data[0]->id_estado; ?>">
                    
                    <div class="input-field col s12">
                        <input type="text" name="nombre" class="validate" value="<?php echo $data[0]->nombre; ?>" pattern="[A-zA-Z ]*"  minlength="3"  tabindex="1" required>
                    </div>
                <br>
                    <div class="form-group">
                        <button class="btn btn-primary">Guardar</button>
                    </div>
                </form>

            </div>

        </div>
    </section>
</div>
</main>