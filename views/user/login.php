
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Inicio Sesion</title>
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:600'><link rel="stylesheet" href="./assets/css/styleLogin.css">
  <link rel="icon" href="assets/img/icono_saywi.ico">

</head>
<body>
<!-- partial:index.partial.html -->
<div class="login-wrap">
			
	<div class="login-html">
			<div class="logo1">
				<img src="assets/img/logo-small.png" alt="">
			</div>
		<div class="loginy">
			<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Iniciar Sesión</label>
			<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Registrarse</label>

			<form onsubmit="return validar()" class="login-form" action="?controller=checklogin" method="post">
		
			<div class="login-form">
			
			<div class="sign-in-htm">
				<div class="group">
					<label for="user" class="label">Usuario</label>
					<input id="user" type="text" class="input" id="usuario" name="usuario" required>
				</div>
				<div class="group">
					<label for="contrasena" class="label">Contraseña</label>
					<input id="txtPassword" type="password" class="input" data-type="password" name="contrasena" required>
				</div>
				<div class="group">
					<input id="check" type="checkbox" class="check" checked required>
					<label for="check"><span class="icon"></span> Terminos y Condiciones</label>
				</div>
				<div class="group">
					<input type="submit" class="button" value="Ingresar">
				</div>
				<div class="hr"></div>
				<div class="foot-lnk">
					<a href="#forgot">Bienvenido a Saywi</a>
				</div>
		
			</form>
			</div>
			<form onsubmit="return validar() class="sign-up-htm" action="?controller=person&method=saveu" method="post">
				<div class="sign-up-htm">
					<div class="group">
						<label for="documento" class="label">Documento</label>
						<input id="documento" type="tel" pattern="[0-9]{3-9}*" placeholder="000000" class="input" name="documento" required >
					</div>
					<div class="group">
						<label for="nombre" class="label">Nombre</label>
						<input id="nombre" type="text" class="input" name="nombre" pattern="[a-zA-Z ]*" required>
					</div>
					<div class="group">
						<label for="telefono" class="label">Teléfono</label>
						<input id="telefono" type="tel" class="input" name="telefono" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" placeholder="320-000-0000" required>
					</div>
					<div class="group">
						<input id="pass" type="hidden" type="text"  value="1"  class="input"  name="id_es_fk" >
					</div>
					<div class="group">
					<input type="submit" class="button" value="Continuar">
					</div>
					<div class="hr"></div>
				</div>
			</form>
		
		</div>
	</div>
</div>
<!-- partial -->
  
</body>
<script type="text/javascript"></script>

</html>

s