
<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
</head>
<main class="container">
<div class="content-wrapper">
    <section class="col-md-12 text-center">
         <div class="col-md-12 m-2 d-flex justify-content-between">
            <h5>USUARIOS</h5>
            <i class="glyphicon glyphicon-plus-sign"></i>
            <a class="btn btn-success" href="?controller=user&method=add">Agregar <i class="mdi mdi-library-plus"></i></a>
        </div>

        <section class="col-md-12 flex-nowrap">
        <table  bgcolor="#d1d1d1"  class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th class="tablita">#</th>
                        <th class="tablita">Usuario</th>
                        <th class="tablita">Tipo Usuario</th>
                        <th class="tablita">Correo</th>
                        <th class="tablita">Estado</th>
                        <th class="tablita">Persona</th>
                        <th class="tablita">Acciones</th>
                    </tr>
                </thead>

                <tbody>

                    <?php foreach ($User as $user) : ?>
                    <tr>
                        <td><?php echo $user->id_usuario ?></td>
                        <td><?php echo $user->usuario ?></td>
                        <td><?php echo $user->tipo_usuario ?></td>
                        <td><?php echo $user->correo ?> </td>
                        <td><?php echo $user->status ?></td>
                        <td><?php echo $user->personas ?></td>
                        <td>
                            <a href="?controller=user&method=edit&id_usuario=<?php echo $user->id_usuario ?>">    <i class="mdi mdi-pencil-circle"></i></a>
                            <a href="?controller=user&method=delete&id_usuario=<?php echo $user->id_usuario ?>">  <i class="mdi mdi-close-circle"></i></a>
                        </td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </section>
    </section>

</main>

</div>

    