
<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
  <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
</head>
<main class="container">
<div class=" formularios">
    <section class="row mt-5">

        <div class="form-horizontal card w-50 m-auto">

            <div class="card-header container">
                <h6 class="m-auto"> AGREGAR USUARIO</h6>
            </div>

            <div class="card-body">
                <form method="POST" action="?controller=user&method=save">


            
                        <div class="gf_right_half">
                            <input type="text" name="usuario" placeholder="Usuario" class="validate" pattern="[a-zA-Z0-9 ]*"  minlength="3"  tabindex="1" required>
                        </div>
                         <div class="gf_right_half">
                            <input type="text" name="tipo_usuario" placeholder="Tipo de usuario" class="form-control" pattern="[a-zA-Z ]*"  minlength="3"  tabindex="1" required>
                        </div>

                        <div class="gf_right_half">
                            <select class="form-control" name="id_per_fk">
                                <option value="">Seleccione la persona</option>
                                <?php foreach ($Person as $p) : ?>
                                    <option value="<?php echo $p->id_personas ?>"><?php echo $p->nombre ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>  
           


                    
                       
                        <div class="gf_left_half">
                            <input type="password" placeholder="Contraseña"name="contrasena" class="validate" pattern="[A-Z]{1-6}+[a-z]{1-6}[0-9]{1-8}*"  minlength="6"
                            maxlength="16" tabindex="1" required>
                        </div>
                        <div class="gf_left_half" data-validate = "Ingrese el correo eléctronico">
                        <input name="correo" type="email" placeholder="Correo electrónico: usuario@dominio.com" class="validate" pattern="[a-zA-Z0-9_]*@[a-zA-Z0-9_]*[.][a-zA-Z]{1,10}"  minlength="5"  tabindex="3" required>
                        </div>
                        <div class="gf_left_half">
                              <select class="form-control" name="id_es_fk">
                                <option value="">Seleccione el estado  en que se encuentra...</option>
                                <?php foreach ($status as $s) : ?>
                                    <option value="<?php echo $s->id_estado ?>"><?php echo $s->nombre ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>          
                        <div class="form-group">
                            
                            <button class="btn btn-primary11">Guardar</button>
                        </div>
                    
                </form>
            </div>
        </div>
    </section>
<div>

</main>