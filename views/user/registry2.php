
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
	<title>Login</title>
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:600'><link rel="stylesheet" href="./assets/css/styleLogin.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/materialize/css/materialize.css">
	<link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="icon" href="assets/img/icono_saywi.ico">
<!-- <script type="text/javascript">
    function validar() {
        //obteniendo el valor que se puso en campo text del formulario
		usuario = document.getElementById("usuario").value;
		txtPasswords = document.getElementById("txtPasswords").value;
		txt_confirmar = document.getElementById("txt_confirmar").value;
		// id_per_fk = document.getElementById("id_per_fk").value;
		// id_es_fk = document.getElementById("id_es_fk").value;
        //la condición

    if (usuario == 0 || txtPasswords == 0 || txt_confirmar == 0 || id_per_fk== 0 || id_es_fk== 0) {
            
            alert('Todos los campos son obligatorios.');
            return false;
    }
	indice = document.getElementById("id_per_fk").selectedIndex;
	indice = document.getElementById("id_es_fk").selectedIndex;
		if( indice == null || indice == 0 ) {
			alert('Selecciona una opción')
			return false;
	}
	txtPasswords = document.getElementById("txtPasswords").value;
	txt_confirmar = document.getElementById("txt_confirmar").value;
	if (txtPasswords != txt_confirmar) {
	alert("Las contraseñas deben de coincidir");
	return false;
	} else {
	alert("Todo esta correcto");
	return true; 
	}
}
</script> -->
<script>
	function mostrarPassword(){
    var cambio = document.getElementById("txtPasswords");
    if(cambio.type == "password"){
        cambio.type = "text";
        $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
    }else{
        cambio.type = "password";
        $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
    }                                                                                
	} 
		$(document).ready(function () {
		//CheckBox mostrar contraseña
		$('#ShowPassword').click(function () {
			$('#Password').attr('type', $(this).is(':checked') ? 'text' : 'password');
		});
	});
</script>
</head>

<body></body>
<!-- partial:index.partial.html -->
<div class="login-wrap">
	<div class="login-html">
    
	<input id="tab-2" type="radio" name="tab" class="sign-up1"><label for="tab-2" class="tab">DATOS DE USUARIO</label>
	
		<form  onsubmit="return validar()" action="?controller=user&method=correo" method="post"  >

			<div class="sign-up1-htm">

				<div class="form-group">
					<label for="user" class="label">Usuario</label>
					<input  type="text" class="input" id="usuario" name="usuario" pattern="[a-zA-Z0-9 ]*" maxlength="40" minlegth="20">
				</div>
				<div class="form-group">
					<label for="contrasena">Contraseña</label>
					<input type="password" id="txtPasswords" name="contrasena" placeholder="Ingrese su contraseña" class="form-control" 
					pattern="[A-Z]{1-6}+[a-z]{1-6}[0-9]{1-8}*" maxlength="16" minlegth="8">
					<div class="input-group-append">
						<button onclick="mostrarPassword()" class="btn btn-primary" type="button"> <span class="fa fa-eye-slash icon"></span> </button>
					</div>
       			 </div>
				<div class="form-group">
					<label for="txt_confirmar" class="label">Confirmar contraseña</label>
					<input type="password" id="txt_confirmar" class="form-control" name="txt_confirmar" placeholder="Confirmar  contraseña" 
					pattern="[A-Z]{1-6}+[a-z]{1-6}[0-9]{1-8}*" maxlength="16" minlegth="8">
       			 </div>
        		<div class="form-group">
					<label for="pass" class="label">Correo</label>
					<input type="email" id="correo" name="correo" class="form-control" placeholder="Ingrese su correo electronico" 
					maxlength="30" minlegth="10">
				</div>
				<div class="form-group">
					<label for="pass" class="label">Confirmar correo</label>
					<input type="email" id="confi_orreo" name="confi_correo" class="form-control" 
					placeholder="usuario@dominio.com" onpaste="true" oncopy="true" maxlength="40" minlegth="20">
				</div>
				<div class="form-group">
          			<input type="hidden" id="tipo" name="tipo_usuario" value="cliente" class="form-control" placeholder="Ingrese el tipo de usuario" >
				</div>
				<div class="form-group">
					<label>Persona</label>
					<select class="form-control"  id="id_per_fk" name="id_per_fk">
						<option value="">Seleccione la persona...</option>
						<?php foreach ($Person as $p) : ?>
						<option value="<?php echo $p->id_personas ?>"><?php echo $p->nombre ?></option>
						<?php endforeach ?>
					</select>
        		</div>

				<div class="form-group">
					<label>Estados</label>
					<select class="form-control" id="id_es_fk" name="id_es_fk">
						<option value="">Seleccione el estado en el que se encuentra...</option>
						<?php foreach ($status as $s) : ?>
						<option value="<?php echo $s->id_estado ?>"><?php echo $s->nombre ?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="form-group">
					<button  type="submit"  name="Registrar"  > Guardar</button>
				</div>
				<div class="hr"></div>
						
				</div>
	</form>
		
	</div>
	</div>
</div>
<!-- partial -->
</body>
</html>
