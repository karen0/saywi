

<head>
<link rel="stylesheet" href="assets/css/estilos.css">  
<link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
</head>

  <link rel="stylesheet" href="assets/css/estilos.css">

<main class="container">
<div class="formularios ">

    <section class="row mt-5">
        <div class="card w-50 m-auto">
            <div class="card-header container">
            <h6 class="m-auto"> EDITAR USUARIO</h6>
            </div>

            <div class="card-body">

                <form method="POST" action="?controller=user&method=update">


                    <input type="hidden" name="id_usuario" class="form-control" value="<?php echo $data[0]->id_usuario; ?>">
                    <div class="carticas">

                    <div class="gf_right_half">
                            <input type="text" name="usuario" class="validate" placeholder="Ingrese su usuario" value="<?php echo $data[0]->usuario; ?>" pattern="[a-zA-Z0-9 ]*" minlength="3"  tabindex="1" required>
                    </div>

                    <div class="gf_right_half">
                            <input type="text" name="tipo_usuario" class="validate" placeholder="Ingrese el tipo de usuario" value="<?php echo $data[0]->tipo_usuario; ?>"  pattern="[a-zA-Z ]*"  minlength="3"  tabindex="1" required>
                    </div>

                    <div class="gf_right_half">
                            <select class="form-control" name="id_per_fk">
                                <option value="">Seleccione la persona...</option>
                                <?php foreach ($Person as $p) : ?>
                                    <option value="<?php echo $p->id_personas ?>"><?php echo $p->nombre ?></option>
                                <?php endforeach ?>
                            </select>
                    </div>


                    </div>
                    <div class="carticas23">
                        <div class="gf_left_half">
                            <input type="password" name="contrasena" class="validate" placeholder="Ingrese su contraseña" value="<?php echo $data[0]->contrasena; ?>" pattern="[A-Z]{1-6}+[a-z]{1-6}[0-9]{1-8}*"   minlength="6" maxlength="16" tabindex="1" required>

                        </div>
                        
                        <div class="gf_left_half">
                            <input type="text" name="correo" class="form-control" placeholder="Correo electrónico: usuario@dominio.com" value="<?php echo $data[0]->correo; ?>"  pattern="[a-zA-Z0-9_]*@[a-zA-Z0-9_]*[.][a-zA-Z]{1,10}"  minlength="5"  tabindex="3" required>
                        </div>
              
                        <div class="gf_left_half">
                            <select class="form-control" name="id_es_fk">
                                <option value="">Seleccione el estado  en que se encuentra...</option>
                                <?php foreach ($status as $s) : ?>
                                    <option value="<?php echo $s->id_estado ?>"><?php echo $s->nombre ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>        

                    <div class="form-group">
                        <button class="btn btn-primary1">Guardar</button>
                    </div>

                </form>


            </div>

        </div>
    </section>


</main>