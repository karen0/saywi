
<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
</head>
<main class="container">
<div class="content-wrapper">
    <section class="col-md-12 text-center">
        <div class="col-md-12 m-2 d-flex justify-content-between">
            <h5>PERSONAS</h5>
            <a class="btn btn-success" href="?controller=person&method=add"> Agregar <i class="mdi mdi-library-plus"></i></a>
        </div>

        <section class="col-md-12 flex-nowrap">
        <table  bgcolor="#d1d1d1"  class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th class="tablita">#</th>
                        <th class="tablita">Documento</th>
                        <th class="tablita">Nombre</th>
                        <th class="tablita">Telefono</th>
                        <th class="tablita">Estado</th>
                        <th class="tablita">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ( $Person as $person) : ?>
                    <tr>
                        <td> <?php echo $person->id_personas ?> </td>
                        <td> <?php echo $person->documento ?> </td>
                        <td> <?php echo $person->nombre ?> </td>
                        <td> <?php echo $person->telefono ?> </td>
                        <td> <?php echo $person->status?> </td> 
                        <td>
                            <a href="?controller=person&method=edit&id_persona=<?php echo $person->id_personas ?>">     <i class="mdi mdi-pencil-circle"></i></a>
                            <a href="?controller=person&method=delete&id_personas=<?php echo $person->id_personas ?>">  <i class="mdi mdi-close-circle"></i></a>
                        </td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </section>
    </section>
</div>
</main>

