
<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
  <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
</head>
<main class="container">
<div class="formularios">
    <section class="row mt-5">
        <div class="card w-50 m-auto">
            <div class="card-header container">
                <h6 class="m-auto">EDITAR PERSONAS</h6>
            </div>
            <div class="card-body">
                <form method="POST" action="?controller=person&method=update">

                    <input type="hidden" name="id_personas" class="form-control" value="<?php echo $data[0]->id_personas; ?>">
            <div class="carticas">

                    <div class="gf_right_half">
                        <input type="number" name="documento" class="validate" placeholder="Documento" value="<?php echo $data[0]->documento; ?>" pattern="[0-9]" required>
                    </div>
                    <div class="gf_right_half">
                        <input type="text" name="telefono" class="validate" placeholder="Teléfono: 320-879-1121" value="<?php echo $data[0]->telefono; ?>"  pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"required>
                    </div>

            </div>
                   
         
                    <div class="gf_left_half2">
                        <input type="text" name="nombre" class="validate" placeholder="Nombre" value="<?php echo $data[0]->nombre; ?>" pattern="[a-zA-Z ]*"  minlength="3"  tabindex="1" required>
                    </div>
                    <div class="gf_left_half2">
                            <select class="form-control" name="id_es_fk" required>
                                <option value="">Seleccione el estado en el que se encuentra...</option>
                            <?php foreach ($status as $s) : ?>
                                <option value="<?php echo $s->id_estado ?>"><?php echo $s->nombre ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                    <div class="form-group">
                        <button class="btn btn-primary2">Guardar</button>
                    </div>
                </form>

           

        </div>
    </section>
</div>
</main>