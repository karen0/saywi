
<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
  <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
</head>
<main class="container">
<div class="formularios">


    <section class="row mt-5">

        <div class="card w-50 m-auto">

            <div class="card-header container">
                <h5z class="m-auto">AGREGAR PERSONA </h5>
            </div>

            <div class="card-body">
                <form method="POST" action="?controller=person&method=save">
            <div class="carticas">
                    <div class="gf_right_half">
                        <input type="number" name="documento" class="validate" placeholder="Documento: 15465879" pattern="[0-9]{3-15}"required>
                    </div>
                    <div class="gf_right_half">
                        <input type="text" name="telefono" class="validate" placeholder="Teléfono: 320-456-0000" type="text" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required>
                    </div>

            </div>
               

                    <div class="gf_left_half2">
                        <input type="text" name="nombre" class="validate" placeholder="Nombre" pattern="[a-zA-Z ]*"  minlength="3"  tabindex="1" required>
                    </div>
                    <div class="gf_left_half2">
                          <select class="form-control" name="id_es_fk">
                            <option value="">Seleccione el estado  en que se encuentra...</option>
                            <?php foreach ($status as $s) : ?>
                                <option value="<?php echo $s->id_estado ?>"><?php echo $s->nombre ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>  
                    </div>
                    
                    <div class="form-group">
                        <button class="btn btn-primary2">Guardar</button>
                    </div>
                </form>

            

        </div>
    </section>
</div>
</main>