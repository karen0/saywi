
<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
</head>
<main class="container">
<div class="content-wrapper">

    <section class="col-md-12 text-center">


        <div class="col-md-12 m-2 d-flex justify-content-between">
            <h4>BORDADOS</h4>
            <a class="btn btn-success" href="?controller=embroidery&method=add"> Agregar<i class="mdi mdi-library-plus"></i></a>
        </div>

        <section class="col-md-12 flex-nowrap">
            <table  bgcolor="#d1d1d1"  class="table table-striped table-hover">

      
                <thead >
                    <tr >
                        <th class="tablita">#</th>
                        <th class="tablita">Nombre</th>
                        <th class="tablita">Imagen</th>
                        <th class="tablita">Medida</th>
                        <th class="tablita">Puntadas</th>
                        <th class="tablita">Precio</th>
                        <th class="tablita">Categoria</th>
                        <th class="tablita">Estado</th>
                        <th class="tablita">Acciones</th>
                    </tr>
                </thead>

                <tbody>

                <?php
                    $conexion = new  mysqli("localhost","root","","saywi_20");
                    if ($conexion){
  
                        }else{
                         echo "insercion no exitosa";
                            }
                        $query="SELECT * FROM bordado";
                $resultado=$conexion->query($query);
             

                ?>
                 <?php foreach ($embroiderys as $embroidery) : ?>
                    <tr>
                         <td> <?php echo $embroidery->id_bordado ?> </td>
                        <td> <?php echo $embroidery->nombre ?></td>
                        <td><img height= "100px" src="data:image/jpg;base64,<?php echo base64_encode($embroidery->imagen);?>"></td>
                        <td> <?php echo $embroidery->medida ?></td>
                        <td> <?php echo $embroidery->puntadas ?></td>
                        <td> <?php echo $embroidery->precio ?></td>
                        <td> <?php echo $embroidery->status ?></td>
                        <td> <?php echo $embroidery->categoria ?></td>
                     <td>
                            <a href="?controller=embroidery&method=delete&id_bordado=<?php echo $embroidery->id_bordado?>"> <i class="mdi mdi-close-circle"></i></a>
                        </td>
                    </tr>
                 <?php endforeach ?>
                  
        </section>
    </section>
</body>
</div>
</main>

               