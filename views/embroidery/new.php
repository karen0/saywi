
<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
  <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
</head>
<main class="container">
<div class="formularios">


    <section class="row mt-5">

        <div class="card w-50 m-auto">

            <div class="card-header container" >
                <h6 class="m-auto">AGREGAR BORDADO</h6>
            </div>

 
            <div class="card-body">
                <form method="POST" action="?controller=procesoB" enctype="multipart/form-data">
        

                <div class="carticas22">
                    <div class="gf_right_half">
                        <input  class="validate" type="text" placeholder="Nombre" name="nombre" pattern="[A-zA-Z ]*"  minlength="3"  tabindex="1" required>      
                    </div>
                    <div class="gf_right_half">  
                        <input  class="validate" type="text" placeholder="Medidas: 15x15" name="medida" pattern="[0-9]{0-10}x[0-9]{0-10}"required>
                    </div>
                    <div class="gf_right_half">  
                        <input  class="validate" type="text" placeholder="Precio" name="precio" pattern="[0-9]{3-10}" required>
                    </div>
                    <div class="gf_right_half">
                        <select class="form-control" name="id_cat_fk" required>
                            <option value="">Seleccione la categoria del bordado...</option>
                            <?php foreach ($categoria as $c) : ?>
                                    <option value="<?php echo $c->id_categoria?>"><?php echo $c->nombre ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>


                </div>
                    
                    <div class="gf_left_half21">
                        <input type="file" name="imagen" placeholder="Ingrese el archivo del bordado." required>
                    </div>
                    <div class="gf_left_half21">  
                        <input  class="validate" type="number" placeholder="Puntadas" name="puntadas" pattern="[0-9]" required>
                    </div>
                    <div class="gf_left_half21">
                        <select class="form-control" name="id_es_fk" required>
                            <option value="">Seleccione el estado en el que se encuentra...</option>
                            <?php foreach ($status as $s) : ?>
                                    <option value="<?php echo $s->id_estado ?>"><?php echo $s->nombre ?></option>
                            <?php endforeach ?>
                        </select>
                        <div class="invalid-feedback">
                            Por favor introduzca el estado.
                        </div>
                        <div class="valid-feedback">
                            Correcto!
                        </div>
                    </div>  
                
                      
              
                    <div class="form-group">
                        <button class="btn btn-primary22">Guardar</button>
                    </div>
                </form>           
             </div>
        </div>
    </section>
</main>