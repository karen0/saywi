
<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
  <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
</head>
<main class="container">
<div class="formularios">

    <section class="row mt-5">

        <div class="card w-50 m-auto">

            <div class="card-header container">
                <h6 class="m-auto">EDITAR BORDADO</h6 >
            </div>

            <div class="card-body">
                <form method="POST" action="?controller=embroidery&method=update">

                    <input type="hidden" name="id_bordado" class="form-control" value="<?php echo $data[0]->id_bordado; ?>">
                <div class="carticas">

                    <div class="gf_right_half">
                        <input type="text" name="nombre" placeholder="Nombre" class="validate" value="<?php echo $data[0]->nombre; ?>" pattern="[A-zA-Z ]*"  minlength="3"  tabindex="1"required>
                    </div>

                    <div class="gf_right_half">
                         <select class="form-control" id="id_cat_fk" >
                                <option value="">Seleccione la categoria en el que se encuentra...
                                </option>
                                    <?php foreach ($categoria as $c) : ?>
                                <option value="<?php echo $c->id_categoria ?>"><?php echo $c->nombre ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                    
                <div class="carticas27">
                    <div class="gf_left_half">
                            <select class="form-control" id="id_es_fk" required>
                                <option value="">Seleccione el estado en el que se encuentra...
                                </option>
                                    <?php foreach ($status as $s) : ?>
                                <option value="<?php echo $s->id_estado ?>"><?php echo $s->nombre 
                                ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="gf_left_half">
                        <input type="file"  name="imagen" class="validate" value="<?php echo $data[0]->foto; ?>" required>
                    </div>
                </div>
                    <div class="form-group">
                        <button class="btn btn-primary2">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

</main>