<!--
Author: W3layouts
Author URL: http://w3layouts.com
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SAYWI</title>
    <link rel="stylesheet" href="assets/css/styleTotal.css">
    <link href="//fonts.googleapis.com/css?family=Roboto:400,700,900&display=swap" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Nunito:400,600,700,800,900&display=swap" rel="stylesheet">
    <link rel="icon" href="assets/img/icono_saywi.ico">

</head>

<body>
    <section class="w3l-bootstrap-header">
        <nav class="navbar navbar-expand-lg navbar-light bg-primary py-lg-2 py-2 px-sm-0 px-2">
            <div class="container">
                <a class="navbar-brand" href="index.html"><img class="navbar-brand" src="assets/img/logo-small.png"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.html">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.html#about">Acerca de Saywi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.html#portfolio">Catálogo</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="index.html#contact">Contáctanos</a>
                        </li>
                    </ul>

                    <div class="form-inline">
                        <a href="?controller=user&method=login" class="btn btn-outline-light btn-outline-action mt-5">Iniciar Sesión</a>
                    </div>

                </div>
            </div>
        </nav>
    </section>

    <section class="w3l-carousel">
        <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active" style="background-image: url('assets/img/nnn.jpg');">
                    <!-- <img src="assets/images/banner.jpg" class="d-block w-100" alt="..."> -->
                    <div class="carousel-caption container">
                        <h3 class="title-cover-9">Costos</h3>
                        <p class="para-cover-9">Tenemos gran variedad en nuestros precios ya que varian de pendiendo de que tan grande sea el bordado, manejamos tamaños desde pequeño, mediano y grande y el tamaño que usted desee.</p>
                        <a href="#portfolio" class="btn btn-primary btn-action mt-5">Ver Portfolio</a>
                        <a href="#about" class="btn btn-outline-light btn-outline-action mt-5">Acerca de Saywi</a>
                    </div>
                </div>
                <div class="carousel-item" style="background-image: url('assets/img/maxresdefault1.jpg')">
                    <!-- <img src="assets/images/banner-3.jpg" class="d-block w-100" alt="..."> -->
                    <div class="carousel-caption container">
                        <h3 class="title-cover-9">Equipo de Trabajo</h3>
                        <p class="para-cover-9">Contamos con un excelente equipo de trabajo los cuales estan calificados para lograr un buen producto y atenderlo de la mejor manera.</p>
                        <a href="#portfolio" class="btn btn-primary btn-action mt-5">Ver Portfolio</a>
                        <a href="#about" class="btn btn-outline-light btn-outline-action mt-5">Acerca de Saywi</a>
                    </div>
                </div>
                <div class="carousel-item" style="background-image: url('assets/img/fff.jpg')">
                    <!-- <img src="assets/images/banner-2.jpg" class="d-block w-100" alt="..."> -->
                    <div class="carousel-caption container">
                        <h3 class="title-cover-9">Maquinas Y Materia Prima</h3>
                        <p class="para-cover-9">Contamos con maquinas de ultima generación y materiales elaborados con los mas altos niveles de calidad garantizando la durabilidad de nuestro producto.</p>
                        <a href="#portfolio" class="btn btn-primary btn-action mt-5">Ver Portfolio</a>
                        <a href="#about" class="btn btn-outline-light btn-outline-action mt-5">Acerca de Saywi </a>
                    </div>
                </div>
                <div class="carousel-item" style="background-image: url('assets/img/team/4.jpg')">
                    <!-- <img src="assets/images/banner-1.jpg" class="d-block w-100" alt="..."> -->
                    <div class="carousel-caption container">
                        <h3 class="title-cover-9">Diseños</h3>
                        <p class="para-cover-9">Tenemos para ti diferentes tipos de diseño diseñados a tu gusto y preferencia, ademas de ello contamos con una gran diseñadora que estara dispuesta a diseñar un unico y original boradado para ti.</p>
                        <a href="#portfolio" class="btn btn-primary btn-action mt-5">Ver Portfolio</a>
                        <a href="#about" class="btn btn-outline-light btn-outline-action mt-5">Acerca de Saywi</a>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>

    <!-- Content-with-photo23 block -->
    <section class="w3l-content-with-photo-23" id="about">
        <div id="cwp23-block" class="py-5">
            <div class="container py-lg-3">
                <h3 class="global-title text-secondary">Acerca de Saywi</h3>
                <div class="row cwp23-content align-items-lg-center">
                    <div class="cwp23-img col-md-6 pt-md-4">
                        <img src="assets/img/3d-rosa-lurex-blumen-stickerei-mit-strass.jpg" class="img-fluid" alt="" />
                    </div>
                    <div class="cwp23-text col-md-6 mt-4 mt-md-0 pl-md-4">
                        <div class="cwp23-title">
                            <h3>Cambia todo tu estilo con un excelente Bordado</h3>
                        </div>
                        <div class="row cwp23-text-cols">
                            <div class="column col-lg-6 mt-4">
                                <h4 class="text-primary">Bordados Personalizados</h4>
                                <p>Bordamos y personalizamos tus prendas más rápido que nuestra competencia. Con la mas alta calidad, esfrozandonos por ser los mejores en nuestro trabajo y cumpliendo con tus espectativas.
                                </p>
                            </div>
                            <div class="column col-lg-6 mt-4">
                                <h4 class="text-primary">Bordados Empresariales</h4>
                                <p>Elaboramos bordados para cualquier tipo de empresas, ya sean empresas grandes, mediadas o pequeñas. Lo importamnte es brindar el mejor bordado para la satisfaccion de tu empresa.  </p>
                            </div>
                            <div class="column col-lg-6 mt-4">
                                <h4 class="text-primary">Bordados Express </h4>
                                <p>Nuestro trabajo es brindar la mejor atencion a nuestros clintes, por ello lo mas inportante para nuestra empresa es brindar bordados de alta calidad en el tiempo requerido por el cliente.</p>
                            </div>
                            <div class="column col-lg-6 mt-4">
                                <h4 class="text-primary">Nuestro Trabajo</h4>
                                <p>Nuestra empresa concede gran importancia a la calidad de nuestros Bordados, por ello nos capacitamos y renovamos dia a dia para generar bordados de alta calidad.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Content-with-photo23 block -->
    </section>
    <!-- portfolio -->
    <section class="w3l-gallery py-5" id="portfolio">
        <div class="container py-lg-3">
            <div class="title-section">
                <h3 class="global-title text-secondary">Catálogo</h3>
            </div>
            <ul class="portfolio-categ filter text-center my-md-5 my-3 p-0">
                <li class="port-filter all active">
                    <a href="#" class="btn btn-primary">Todos</a>
                </li>
                <li class="cat-item-1">
                    <a href="#" class="btn btn-outline-primary" title="Category 1">Deportivos</a>
                </li>
                <li class="cat-item-2">
                    <a href="#" class="btn btn-outline-primary" title="Category 2">Escudos</a>
                </li>
                <li class="cat-item-3">
                    <a href="#" class="btn btn-outline-primary" title="Category 3">Logos</a>
                </li>
            </ul>
            <ul class="portfolio-area clearfix p-0 m-0">
                <li class="portfolio-item2" data-id="id-1" data-type="">
                    <span class="image-block">
                        <a class="image-zoom" href="assets/img/20191112_210148.jpg" data-gal="prettyPhoto[gallery]">
                            <img src="assets/img/karen1.jpg" class="img-fluid w3layouts " alt="portfolio-img">
                        </a>
                    </span>
                </li>
                <li class="portfolio-item2" data-id="id-2" data-type="">
                    <span class="image-block">
                        <a class="image-zoom" href="assets/img/20191112_210216.jpg" data-gal="prettyPhoto[gallery]">
                            <img src="assets/img/karen4.jpg" class="img-fluid w3layouts " alt="portfolio-img">
                        </a>
                    </span>
                </li>
                <li class="portfolio-item2" data-id="id-3" data-type="">
                    <span class="image-block">
                        <a class="image-zoom" href="assets/img/20191112_210918.jpg" data-gal="prettyPhoto[gallery]">
                            <img src="assets/img/img5.jpg" class="img-fluid w3layouts " alt="portfolio-img">
                        </a>
                    </span>
                </li>
                <li class="portfolio-item2" data-id="id-4" data-type="cat-item-1">
                    <span class="image-block">
                        <a class="image-zoom" href="assets/img/categoriaFlores.jpeg" data-gal="prettyPhoto[gallery]">
                            <img src="assets/img/no.jpg" class="img-fluid w3layouts " alt="portfolio-img">
                        </a>
                    </span>
                </li>
                <li class="portfolio-item2" data-id="id-5" data-type="">
                    <span class="image-block">
                        <a class="image-zoom" href="assets/img/futbol.jpg" data-gal="prettyPhoto[gallery]">
                            <img src="assets/img/futbol.jpg" class="img-fluid w3layouts " alt="portfolio-img">
                        </a>
                    </span>
                </li>
                <li class="portfolio-item2" data-id="id-7" data-type="">
                    <span class="image-block">
                        <a class="image-zoom" href="assets/img/20191112_210723.jpg" data-gal="prettyPhoto[gallery]">
                            <img src="assets/img/panda.jpg" class="img-fluid w3layouts " alt="portfolio-img">
                        </a>
                    </span>
                </li>
                <li class="portfolio-item2" data-id="id-4" data-type="cat-item-2">
                    <span class="image-block">
                        <a class="image-zoom" href="assets/img/20191112_210743.jpg" data-gal="prettyPhoto[gallery]">
                            <img src="assets/img/oli.jpg" class="img-fluid w3layouts " alt="portfolio-img">
                        </a>
                    </span>
                </li>
                <li class="portfolio-item2" data-id="id-5" data-type="cat-item-3">
                    <span class="image-block">
                        <a class="image-zoom" href="assets/img/20191112_211028.jpg" data-gal="prettyPhoto[gallery]">
                            <img src="assets/img/toyo.jpg" class="img-fluid w3layouts " alt="portfolio-img">
                        </a>
                    </span>
                </li>
                <li class="portfolio-item2" data-id="id-7" data-type="cat-item-3">
                    <span class="image-block">
                        <a class="image-zoom" href="assets/img/20191112_210448.jpg" data-gal="prettyPhoto[gallery]">
                            <img src="assets/img/nike.jpg" class="img-fluid w3layouts " alt="portfolio-img">
                        </a>
                    </span>
                </li>
                <div class="clear"></div>
            </ul>
            <!--end portfolio-area -->
        </div>
        <!-- //gallery container -->
    </section>
    <!-- //portfolio -->

    <section class="form-16" id="booking">
        <!-- /form-16-section -->
        <div class="form-16-mian py-5">
            <div class="container py-lg-3">
                <div class="section-title align-center text-center">
                    <h3 class="global-title text-white">Regístrate y Empieza tu Cotización</h3>
                </div>
                <h4>Para conocer mas sobre nuestros bordados y empezar tu cotizacion regístrate y sigue los pasos.</h4>
                <div class="forms-16-top">

                    <div class="form-right-inf">
                        <div class="form-inner-cont">

                            <form action="#" method="post" class="signin-form">
                                <div class="d-grid book-form">

                                    <button class="btn btn-primary btn-book">¡COTIZA YA!</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- //form-16-section -->
    <div class="w3l-grids-block-5" id="blog">
        <!-- grids block 5 -->
        <section id="grids5-block" class="py-5">
            <div class="container py-lg-3">
                <div class="section-title align-center text-center">
                    <h3 class="global-title text-secondary">Tendencia En Bordados</h3>
                </div>
                <div class="row">
                    <div class="grids5-info col-lg-4 col-md-6 mt-4">
                        <a href="blog-single.html"><img src="assets/img/bonita.jpg" alt=/></a>
                        <div class="blog-info">
                            <h4><a href="blog-single.html">Bordados Empresariales</a></h4>
                            <p>Te ofrecemos bordar infinidad de prendas y logotipos, nombres y dibujos..</p>
                        </div>
                    </div>
                    <div class="grids5-info col-lg-4 col-md-6 mt-4">
                        <a href="blog-single.html"><img src="assets/img/colegios.jpg" alt="" /></a>
                        <div class="blog-info">
                            <h4><a href="blog-single.html">Bordados Colegiales</a></h4>
                            <p>Elaboramos los escudos colegiales incluyendo las chaquetas de once.</p>
                        </div>
                    </div>
                    <div class="grids5-info col-lg-4 offset-md-3 offset-lg-0 col-md-6 mt-4">
                        <a href="blog-single.html"><img src="assets/img/team/6.jpg" alt="" /></a>
                        <div class="blog-info">
                            <h4><a href="blog-single.html">Bordados en General</a></h4>
                            <p>Bordamos y personalizamos tus prendas más rápido que nuestra competencia. .</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- // grids block 5 -->
    <!-- contacts -->
    <section class="w3l-contacts-9-main" id="contact">
        <div class="contacts-9 py-5">
            <div class="container py-lg-3">
                <h3 class="global-title text-secondary">Contáctenos</h3>
                <div class="row top-map">
                    <div class="cont-details col-md-5">
                        <div class="cont-top">
                            <div class="cont-left">
                                <span class="fa fa-phone text-primary"></span>
                            </div>
                            <div class="cont-right">
                                <h6>Llámenos</h6>
                                <p><a href="tel:+57 321 355 9727">+57 321 355 9727</a></p>

                            </div>
                        </div>
                        <div class="cont-top mt-lg-5 mt-4">
                            <div class="cont-left">
                                <span class="fa fa-envelope-o text-primary"></span>
                            </div>
                            <div class="cont-right">
                                <h6>Escríbenos</h6>

                                <p><a href="saywi20@gmail.com" class="mail">saywi20@gmail.com</a></p>
                            </div>
                        </div>
                        <div class="cont-top mt-lg-5 mt-4">
                            <div class="cont-left">
                                <span class="fa fa-map-marker text-primary"></span>
                            </div>
                            <div class="cont-right">
                                <h6>Encuéntranos</h6>
                                <p>Barrio: Candelaria la Nueva</p>
                            </div>
                        </div>
                    </div>
                    <div class="map-content-9 col-md-7 mt-5 mt-md-0">
                        <form action="https://sendmail.w3layouts.com/submitForm" method="post">
                            <div class="twice-two">
                                <input type="text" class="form-control" name="w3lName" id="w3lName" placeholder="Nombre" required="">
                                <input type="email" class="form-control" name="w3lSender" id="w3lSender" placeholder="Correo" required="">
                            </div>
                            <div class="twice">

                                <input type="text" class="form-control" name="w3lName" id="w3lName" placeholder="Asunto" required="">
                            </div>
                            <textarea name="w3lMessage" class="form-control" id="w3lMessage" placeholder="Mensaje" required=""></textarea>
                            <button type="submit" class="btn btn-primary btn-contact">Enviar Mensaje</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <!-- //contacts -->

    <!-- footer -->
    <section class="w3l-footer-29-main" id="footer">
        
        
            <div class="container pb-lg-3">
                <div class="row bottom-copies">
                    <p class="copy-footer-29 col-lg-8">© 2020 SAYWI. Todos los derechos reservados
                        <ul class="list-btm-29 col-lg-4">
                            
                            <li><a href="#link">Politica de Privacidad</a></li>
                            <li><a href="#link">Terminos de servicio</a></li>

                        </ul>
                </div>
            </div>
        
        <!-- move top -->
        <button onclick="topFunction()" id="movetop" class="bg-primary" title="Go to top">
        <span class="fa fa-angle-up"></span>
    </button>
        <script>
            // When the user scrolls down 20px from the top of the document, show the button
            window.onscroll = function() {
                scrollFunction()
            };

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("movetop").style.display = "block";
                } else {
                    document.getElementById("movetop").style.display = "none";
                }
            }

            // When the user clicks on the button, scroll to the top of the document
            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }
        </script>
        <!-- /move top -->
    </section>
    <!-- // footer -->

    <!-- jQuery -->
    <script src="assets/js/jquery-3.4.1.slim.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- disable body scroll which navbar is in active -->
    <script>
        $(function() {
            $('.navbar-toggler').click(function() {
                $('body').toggleClass('noscroll');
            })
        });
    </script>
    <!-- disable body scroll which navbar is in active -->

    <!-- jQuery-Photo-filter-lightbox-portfolio-plugin -->
    <script src="assets/js/jquery-1.7.2.js"></script>
    <script src="assets/js/jquery.quicksand.js"></script>
    <script src="assets/js/script.js"></script>
    <script src="assets/js/jquery.prettyPhoto.js"></script>
    <!-- jQuery-Photo-filter-lightbox-portfolio-plugin -->
</body>

</html>