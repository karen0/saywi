<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
  <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
</head>
<main class="container">
<div class="content-wrapper">
<main class="container">

    <div class="row">
        <h2 class="col-12 d-flex justify-content-center">Cotizacion</h2>
    </div>

    <section class="row mt-5">

        <div class="card w-50 m-auto">

            <div class="card-header container">
                <h4 class="m-auto">EDITAR Cotizacion</h4>
            </div>

            <div class="card-body">
                <form method="POST" action="?controller=quote&method=update">

                    <input type="hidden" name="	id_cotizacion" class="form-control" value="<?php echo $data[0]->id_cotizacion; ?>">

                    <div class="input-field col s12">
                        <label for="fecha_pedi">Fecha pedido</label>
                        <input type="date" name="fecha_pedi" class="validate" value="<?php echo $data[0]->fecha_pedi; ?>" required>
                    </div>
                <br>
                    <div class="input-field col s12">
                    <label for="fecha_entrega">Fecha entrega</label>
                        <input type="date" name="fecha_entrega" class="validate" value="<?php echo $data[0]->fecha_entrega; ?>" required>
                    </div>
                <br>
                    <div class="input-field col s12">
                        <label for="observacion">Observación</label>
                        <input type="text" name="observacion" class="validate"  value="<?php echo $data[0]->observacion; ?>" pattern="[A-Za-z]+"  minlength="3"  tabindex="1" required>
                    </div>   
                <br>
                    <div class="input-field col s12">
                            <select class="form-control" name="id_es_fk" required>
                                <option value="">Seleccione el estado en el que se encuentra...</option>
                            <?php foreach ($status as $s) : ?>
                                <option value="<?php echo $s->id_estado ?>"><?php echo $s->nombre ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                <br>
                    <div class="form-group">
                        <button class="btn btn-primary">Guardar</button>
                    </div>
                </form>

            </div>

        </div>
    </section>
   </div>
</main>
