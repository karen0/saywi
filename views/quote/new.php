<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
  <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
</head>
<main class="container">
<div class="formularios">
    

    <section class="row mt-5">

        <div class="card w-50 m-auto">

            <div class="card-header container">
                <h5 class="m-auto">AGREGAR SOLICITUD</h5>
            </div>

            <div class="card-body">
                <form method="POST" action="?controller=quote&method=save">

                    <div class="input-field col s12">
                        <input type="date" id="fecha_pedi" class="validate" required>
                        <label for="fecha_pedi">Fecha pedido; DD-MM-AAAA</label>
                    </div>
                <br>
                    <div class="input-field col s12">
                        <input type="date" id="fecha_entrega" class="validate" placeholder="Fecha entrega" autofocus title="Ingrese  la fecha de entrega así: DD-MM-AAAA"required>
                        <label for="fechaEntrega">Fecha entrega</label>
                    </div>
                <br>
                    <div class="input-field col s12">
                        <input type="text" class="validate" id="observacion" pattern="[A-Za-z]+"  minlength="3"  tabindex="1" >
                        <label for="observacion">Observación: Solo con letras.</label>
                    </div>
                <br>
                    <div class="input-field col s12">
                        <select class="form-control" name="id_es_fk" required>
                            <option value="">Seleccione el estado en el que se encuentra...</option>
                            <?php foreach ($status as $s) : ?>
                                    <option value="<?php echo $s->id_estado ?>"><?php echo $s->nombre ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>  
                <br>                   
                    <div class="form-group">
                        <button class="btn btn-primary">Guardar</button>
                    </div>
                </form>

            </div>

        </div>
    </section>
   </div>
</main>
