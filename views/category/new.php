
<head>
  <link rel="stylesheet" href="assets/css/estilos.css">
  <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
</head>
<main class="container">
<div class="formularios">

    <section class="row mt-5">

        <div class="card w-50 m-auto">

            <div class="card-header container">
                <h6 class="m-auto">AGREGAR CATEGORÍA</h6> 
            </div>

            <div class="card-body">
                <form method="POST" action="?controller=proceso" enctype="multipart/form-data">

            <div class="carticas">
                    <div class="gf_right_half" data-validate = "Ingrese el nombre">
                        <input  id="nombre" class="validate" placeholder="Nombre de la categoría" name ="nombre" type="text" pattern="[A-zA-Z ]*"  minlength="3"  tabindex="1" required>
                    </div>
                    <div class="gf_right_half"> 
                            <select class="form-control" name="id_es_fk">
                                <option value="">Seleccione el estado  en que se encuentra...</option>
                                <?php foreach ($status as $s) : ?>
                                    <option value="<?php echo $s->id_estado ?>"><?php echo $s->nombre ?></option>
                                <?php endforeach ?>
                            </select>   
                    </div> 
            </div>
                    <div class="gf_left_half23">                        
                        <input type="file" name="imagen" placeholder="Ingrese el archivo de la categoria" required>
                    </div>
                   
                    <div class="form-group">
                        <button class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            
        </div>
    </section>
</main>