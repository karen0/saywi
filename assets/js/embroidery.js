var arrayCotizacion = []

$(document).ready(function() {

    $('#form').submit(function(ev) {
        ev.preventDefault()
    })

    $('#add').click(function(ev) {
        ev.preventDefault()
        let category = $('#category option:div')
        let id_categoria = category.val()
        let nombre = (category.text()).trim()

        let exist = arrayCotizacion.find(element => {
            return id_categoria == element[0].id
        })

        if (exist === undefined) {
            arrayCotizacion.push([{
                'id_categoria': id_categoria,
                'nombre': nombre
            }])

            $('#infoCategories').append("<div class='row m-4'><div class='col-md-4'><p class='text-center'>" + nombre + "</p></div><div class='col-md-6'><button onclick='remove(" + id_categoria + ")' class='btn btn-danger'>Borrar</button></div></div>")
        } else
            alert("La categoría ya se ha agregado.")

    })

    $('#category').change(function() {
        if ($('#category').val() != '')
            $('#add').show()
        else
            $('#add').hide()
    })

    $('#submit').click(function() {
        let data = {
            'nombre': $('#name').val(),
            'id_es_fk': $('#status').val(),
            'arrayCotizacion': arrayCotizacion
        }

        $.post("?controller=movie&method=save", data, function(response) {
            window.location = '?controller=movie'
        }).fail(function() {
            alert('Error, No se puedo ')
        })
    })

})

function remove(idCategory) {
    let exist = arrayCotizacion.find(element => {
        return idCategory == element[0].id
    })

    index = arrayCotizacion.indexOf(exist)
    arrayCotizacion.splice(index, 1)

    $('#infoCategories').html('')

    arrayCotizacion.forEach(function(element) {
        $('#infoCategories').append("<div class='row m-4'><div class='col-md-4'><p class='text-center'>" + element[0].nombre + "</p></div><div class='col-md-6'><button onclick='remove(" + element[0].id + ")' class='btn btn-danger'>Borrar</button></div></div>")
    })
}