var arrayStatus = []

$(document).ready(function() {  	
	
	$('#form').submit(function (ev) {
		ev.preventDefault()
	})

	$('#add').click(function (ev) {
		ev.preventDefault()
		let status = $('#status option:selected')
		let id_estado = status.val()
		let nombre = (status.text()).trim()

		let exist = arrayStatus.find( element => {
			return id_estado == element[0].id_estado
		})

		if(exist === undefined) {			
			arrayStatus.push([
					{
						'id_estado': id_estado,
						'nombre': nombre
					}
				])
			
			$('#infoCategories').append("<div class='row m-4'><div class='col-md-4'><p class='text-center'>"+nombre+"</p></div><div class='col-md-6'><button onclick='remove("+id_estado+")' class='btn btn-danger'>Borrar</button></div></div>")
		}
		else 
			alert("El estado ya se ha agregado.")

	})

	$('#status').change(function() {
		if($('#status').val() != '')
			$('#add').show()
		else
			$('#add').hide()
	})

	$('#submit').click(function () {
		let data = {
			'name': $('#name').val(),
			'user_id': $('#user').val(),
			'arrayStatus': arrayStatus
		}

		$.post("?controller=usuarios&method=save", data, function (response) {
			window.location = '?controller=usuarios'
		}).fail(function () {
			alert('Error, No se puedo Registrar la Pelicula')
		})
	})

})

function remove(id_estado) {
	let exist = arrayStatus.find( element => {
			return id_estado == element[0].id
		})

	index = arrayStatus.indexOf(exist)
	arrayStatus.splice(index,1)
	
	$('#infoStatus').html('')
	
	arrayStatus.forEach(function(element){
		$('#infoStatuss').append("<div class='row m-4'><div class='col-md-4'><p class='text-center'>"+element[0].nombre+"</p></div><div class='col-md-6'><button onclick='remove("+element[0].id_estado+")' class='btn btn-danger'>Borrar</button></div></div>")
	})
}