$("#btn-s").hide()

var arr_bordados = []
var id_persona = 0
function add_shop(idpersonfk,id_bordado, nombre, precio) {
    id_persona = idpersonfk
    arr_bordados.push({
        'id_bordado': id_bordado,
        'nombre': nombre,
        'cantidad': $("#cantidad"+id_bordado).val(),
        'precio': precio
    })
    $('#btn-add'+id_bordado).hide()
    if(arr_bordados.length > 0){
        $("#btn-s").show()
    }
}

$("#btn-s").click(function(){
    if(arr_bordados.length > 0){
        $("#FinCoti").show()
        $("#listBordados").hide()
        var totalPagar = 0
        for(var i=0; i<arr_bordados.length; i++) {
            total = parseInt(arr_bordados[i]['precio'])*parseInt(arr_bordados[i]['cantidad']);
            $("#tbody").append("<tr><td>"+arr_bordados[i]['nombre']+"</td><td>"+arr_bordados[i]['cantidad']+"</td><td>"+arr_bordados[i]['precio']+"</td><td>"+total+"</td></tr>")
            totalPagar = totalPagar + total
        }
        $('#total').append(totalPagar)
    }        
})

$("#enviar").click(function(){
    var datos = {
        "id_persona": id_persona,
        "fecha_entrega": $("#fecha_entrega").val(),
        "totalPagar": $("#totalPagar").val(),
        "arr_bordados": arr_bordados
    }

    $.ajax({
        method: "POST",
        url: "?controller=embroidery&method=mostrarb",
        data: datos
    }).done(function(resp){
        alert("Cotización Generada")
    })
})