<?php
require 'models/Status.php';
require 'models/embroidery.php';
require 'models/quote.php';

class DetailController
{
    private $model;
    private $modelStatus;
    private $modelEmbroidery;
    private $modelQuote;

    public function __construct()
    {
        $this->model = new Detail;
        $this->modelStatus = new Status;
        $this->modelEmbroidery = new Embroidery;
        $this->modelQuote = new Quote;
    }

    public function index()
    {
        require 'views/layout.php';
        $quote = $this->model->getAll();
        require 'views/category/list.php';
    }

    public function mostrarb()
    {
        if (isset($_REQUEST['id_categoria'])) {
            $id_bordado = $_REQUEST['id_bordado'];
            $this->model->nuevaCoti($id_bordado);
            $data = $this->model->getByIdm($id_bordado);
            $embroiderym = $this->modelEmbroidery->getAllm($id_categoria);
            require 'views/embroidery/tipo1.php';
        } else {
            echo "Error";
        }
    }
}
?>