<?php

require 'models/User.php';
require 'models/Person.php';
require 'models/Status.php';
require 'models/category.php';

class UserController
{

  private $modelUser;
  private $modelPerson;
  private $modelStatus;

  public function __construct()
  {
    $this->modelUser = new User;
    $this->modelPerson = new Person;
    $this->modelStatus = new Status;
  }

  public function index()
  {
    require 'views/layout.php';
    $User = $this->modelUser->getAll();
    require 'views/user/list.php';
  }


  public function hola()
  {
   
    require 'views/home2.php';
  }
  
  public function gerente()
  {
    require 'views/layout.php';
    require 'views/home.php';
  }
  public function index1()
  {
    require 'views/user/login.php';
    $User = $this->modelUser->getAll();
  }
  public function login()
  {
    require 'views/user/login.php';
    $User = $this->modelUser->getAll();
    
  }

  public function cliente()
  {
    require 'views/home3.php'; 
  }

  public function correo()
  {
    require 'views/user/PhpMailer/correo.php'; 
  }
  public function logout()
  {
    require 'controllers/logout.php'; 
  }
  public function cerrar()
  {
    require 'views/home2.php';  
  }
  public function add()
  {
    require 'views/layout.php';
    $Person = $this->modelPerson->getAll();
    $status = $this->modelStatus->getAll();
    require 'views/user/new.php';
  }

  public function save()
  {
    $this->modelUser->newUser($_REQUEST);
    header('Location: ?controller=user&method=index');
  }
  public function savep()
  {
    $this->modelUser->newUser($_REQUEST);
    header('Location: ?controller=user&method=correo');
  }

  public function edit()
  {

    if (isset($_REQUEST['id_usuario'])) {
      $id_usuario = $_REQUEST['id_usuario'];
      $data = $this->modelUser->getById($id_usuario);
      $Person = $this->modelPerson->getAll();
      $status = $this->modelStatus->getAll();
      require 'views/layout.php';
      require 'views/user/edit.php';
    } else {
      echo "Error";
    }
  }

  public function update()
  {
    if (isset($_POST)) {
      $this->modelUser->editUser($_POST);
      header('Location: ?controller=user');
    } else {
      echo "Error";
    }
  }

  public function delete()
  {
    $this->modelUser->deleteUser($_REQUEST);
    header('Location: ?controller=user');
  }

  

} # fin clase UsuarioController
