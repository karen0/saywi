<?php

require 'models/Quote.php';
require 'models/Status.php';

class QuoteController
{

    private $model;
    private $modelStatus;

    public function __construct()
    {
        $this->model = new Quote;
        $this->modelStatus = new Status;
    }

    public function index()
    {

        require 'views/layout.php';
        $Quote = $this->model->getAll();
        require 'views/quote/list.php';
    }


   public function quote()
  {
    $Quote = $this->model->getAll();
    $status = $this->modelStatus->getAll();
    require 'views/quote/new.php';
  }


    public function add()
    {
        require('views/layout.php');
        $status = $this->modelStatus->getAll();
        require('views/quote/new.php');
    }
   
    public function save()
    {
        $this->model->newQuote($_REQUEST);
        header('Location: ?controller=category&method=catalogo');
    }




    public function edit()
    {

        if (isset($_REQUEST['id_cotizacion'])) {
            $id_cotizacion = $_REQUEST['id_cotizacion'];
            $data = $this->model->getById($id_cotizacion);
            $status = $this->modelStatus->getAll();
            require('views/layout.php');
            require('views/quote/edit.php');
        }else{
            echo "Error";
        }
    }

    public function update()
    {
        if(isset($_POST)){
            $this->model->editQuote($_POST);
            header('Location: ?controller=quote');
        }else{
            Echo "error";
        } 
    }

    public function delete()
    {
        $this->model->deleteQuote($_REQUEST);
        header('Location: ?controller=quote');
    }
}