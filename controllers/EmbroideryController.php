<?php

require 'models/embroidery.php';
require 'models/Status.php';
require 'models/category.php';

class EmbroideryController
{

  private $model;
  private $modelStatus;
  private $modelCategory;

  public function __construct()
  {
    $this->model = new Embroidery;
    $this->modelStatus = new Status;
    $this->modelCategory = new category;
  }

  public function index()
  {

    require 'views/layout.php';
    $embroiderys = $this->model->getAll();
    require 'views/embroidery/list.php';
  }

  public function add()
  {
    require 'views/layout.php';
    $status = $this->modelStatus->getAll();
    $categoria = $this->modelCategory->getAll();
    require 'views/embroidery/new.php';
    
  }

  public function save()
  {
    $this->model->newbordado($_REQUEST);
    header('Location: ?controller=embroidery&method=index');
  }

  public function edit()
  {

    if (isset($_REQUEST['id'])) {
      $id = $_REQUEST['id'];
      $data = $this->model->getById($id);
      $status = $this->modelStatus->getAll();
      $categoria = $this->modelCategory->getAll();
      require 'views/layout.php';
      require 'views/embroidery/edit.php';
    } else {
      echo "Error";
    }
  }

  public function update()
  {
    if (isset($_POST)) {
      $this->model->editbordado($_POST);
      header('Location: ?controller=embroidery');
    } else {
      echo "Error";
    }
  }

  public function delete()
  {
    $this->model->deletebordado($_REQUEST);
    header('Location: ?controller=embroidery');
  }
  public function mostrarb()
  {    
    $fecha_entrega = $_REQUEST['fecha_entrega'];
    $totalPagar = $_REQUEST['totalPagar'];
    $arr_bordados = $_REQUEST['arr_bordados'];
    $id_persona = $_REQUEST['id_persona'];
    $id_cotizacion = $this->modelCategory->nuevaCotizacion($id_persona, $fecha_entrega);
    foreach($arr_bordados as $bordado) {
      $categoria = $this->modelCategory->insertar($id_cotizacion, $bordado);
    }   
  }
}