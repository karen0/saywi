<?php

require 'models/category.php';
require 'models/Status.php';
require 'models/embroidery.php';

class CategoryController
{

  private $model;
  private $modelStatus;
  private $modelEmbroidery;

  public function __construct()
  {
    $this->model = new Category;
    $this->modelStatus = new Status;
    $this->modelEmbroidery = new Embroidery;
  }

  public function index()
  {
    require 'views/layout.php';
    $categoria = $this->model->getAll();
    require 'views/category/list.php';
  }
   public function quote()
  {
    $categoria = $this->model->getAll();
    require 'views/category/catalogo.php';
  }

  public function catalogo()
  {
    $categoria = $this->model->getAll();
    require 'views/category/catalogo.php';
  }

  public function add()
  {
      require 'views/layout.php';
      $status = $this->modelStatus->getAll();
      require 'views/category/new.php';
      

  }

  public function reporte()
  {
     
      require 'views/reportes/index.php';
      
  }
  public function save()
  {
    $this->model->newcategory($_REQUEST);
    header('Location: ?controller=category&method=index');
  }
  public function editc()
  {
    if (isset($_REQUEST['id_categoria'])) {
      $id = $_REQUEST['id_categoria'];
      $data = $this->model->getById($id);
      $status = $this->modelStatus->getAll();
      require 'views/layout.php';
      require 'views/category/edit.php';
    } else {
      echo "Error";
    }
  }
  public function edit()
  {

    if (isset($_REQUEST['id_categoria'])) {
      $id = $_REQUEST['id_categoria'];
      $data = $this->model->getById($id);
      $status = $this->modelStatus->getAll();
      require 'views/layout.php';
      require 'views/category/edit.php';
    } else {
      echo "Error";
    }
  }

  public function update()
  {
    if (isset($_POST)) {
      $this->model->editcategory($_POST);
      header('Location: ?controller=category');
    } else {
      echo "Error";
    }
  }

  public function delete()
  {
    $this->model->deletecategory($_REQUEST);
    header('Location: ?controller=category');
  }

  

  public function mostrar()
  {
    if (isset($_REQUEST['id_categoria'])) {
      $idPersona = $_REQUEST['idpersonfk'];
      $_SESSION['id_persona'] = $idPersona;
      $guardar = $this->model->getidCotizacion($idPersona);
      $_SESSION ['idcotizacion']=$guardar;
      $id_categoria = $_REQUEST['id_categoria'];
      $data = $this->model->getByIdm($id_categoria);
      $embroiderys = $this->modelEmbroidery->getAllm($id_categoria);
      require 'views/embroidery/tipo1.php';
    
    } else {
      echo "Error";
    }
  }
}


