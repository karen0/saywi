<?php

require 'models/Person.php';
require 'models/Status.php';

class PersonController
{

    private $model;
    private $modelStatus;

    public function __construct()
    {
        $this->model = new Person;
        $this->modelStatus = new Status;
    }

    public function index()
    {

        require 'views/layout.php';
        $Person = $this->model->getAll();
        require 'views/person/list.php';
    }
    public function index1()
    {

        require 'views/layout.php';
        $Person = $this->model->getAll();
    }

    public function add()
    {
        require('views/layout.php');
        $status = $this->modelStatus->getAll();
        require('views/person/new.php');
    }
    public function save()
    {
        $this->model->newPerson($_REQUEST);
        header('Location: ?controller=person&method=index');
    }
    public function saveu()
    {
        $Person = $this->model->getAll();
        $status = $this->modelStatus->getAll();
        $this->model->newPerson($_REQUEST);
        require 'views/user/registry2.php';
    }

    public function correo()
    {
        $Person = $this->model->getAll();
        $status = $this->modelStatus->getAll();
        $this->model->newPerson($_REQUEST);
        require 'views/user/correo.php';
    }

    public function edit()
    {

        if (isset($_REQUEST['id_persona'])) {
            $id_personas = $_REQUEST['id_persona'];
            $data = $this->model->getById($id_personas);
            $status = $this->modelStatus->getAll();
            require('views/layout.php');
            require('views/person/edit.php');
        }else{
            echo "Error";
        }
    }


    public function update()
    {
        if(isset($_POST)){
            $this->model->editPerson($_POST);
            header('Location: ?controller=person');
        }else{
            Echo "error";
        } 
    }

    public function delete()
    {
        $this->model->deletePerson($_REQUEST);
        header('Location: ?controller=person');
    }
}